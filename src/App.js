import React from 'react';
import countries from './countries';
import Country from './components/country';
import './App.css';

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            countries: countries
        }
    }

    removeCountry = (index) => {
        //Clonar la lista de objetos
        let countriesClone = [...this.state.countries];
        //Eliminamos el elemento del arreglo
        countriesClone.splice(index, 1);
        //Actualizamos el estado
        this.setState({countries: countriesClone});
    }

    toggleContextMenu = (index) => {
        //Clonar la lista de objetos
        let countriesClone = JSON.parse(JSON.stringify(this.state.countries));
        //Actualizamos el valor de la propiedad selected
        countriesClone[index].showContextM = !countriesClone[index].showContextM;
        //Actualizamos el estado
        this.setState({countries: countriesClone});
    }

    render() {

        const contextFn = {
            toggleContextMenuFn: this.toggleContextMenu,
            removeCountryFn: this.removeCountry,
        }

        return (
            <div className="App">
                {
                  this
                    .state
                    .countries
                    .map((country, index) => {
                        return (
                            <Country
                            key={country.code}
                            index={index}
                            selected={country.selected}
                            name={country.name}
                            code={country.code}
                            showContextM={country.showContextM}
                            contextFn={contextFn}
                            /> 
                        )
                    })
                }
            </div>
        );
    }
}

export default App;
