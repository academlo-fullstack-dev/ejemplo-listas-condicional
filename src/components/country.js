import React from 'react';
import {DotsVerticalOutline} from '@graywolfai/react-heroicons';
import ContextMenu from './context-menu';

export default function Country(props) {
    return (
        //1. Operador condicional (ternaria) <div className={`country ${props.selected ? 'selected' : '' }`} onClick={() => props.selectedFn(props.index)}></div>
        //2. Operador lógico &&
        <div className={`country ${props.selected && 'selected'}`}>
            <div className="contextual-container">
                <div className="contextual-menu" onClick={() => props.contextFn.toggleContextMenuFn(props.index)}>
                    <DotsVerticalOutline />
                </div>
                {
                    //Renderizado condicional con operador ternario y operador condicional &&
                }
                {
                    props.showContextM ? (<ContextMenu removeFn={props.contextFn.removeCountryFn} index={props.index} />) :  null
                }
                {
                    /* props.showContextM && <ContextMenu /> */
                }
            </div>
            <img
                src={`https://www.countryflags.io/${props.code}/flat/64.png`}
                alt={props.name}/>
            <h4>{props.name}</h4>
        </div>
    )
}